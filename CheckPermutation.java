package com.StringPermutation;

import java.util.Scanner;

public class CheckPermutation {
	
	public static void main(String[] args){
		@SuppressWarnings("resource")
		Scanner scanner=new Scanner(System.in);
		StringBuilder string=new StringBuilder();
		System.out.print("Enter any String: ");
		string.append(scanner.next());
		permutation(string,0,string.length());
		
	}

	 static void permutation(StringBuilder string, int initial, int length) {
		 
		if(initial==length-1){
			System.out.println(string);
		}else{
			for(int j=initial;j<length;j++){
				swap(string,initial,j);
				permutation(string,initial+1,length);
				swap(string,initial,j);
			}
		}
	 }

	 static void swap(StringBuilder string, int i, int j) {
		String tmp,tmp2;
		tmp=string.substring(i,i+1);
		tmp2=string.substring(j,j+1);
		string.replace(i, i+1,tmp2);
		string.replace(j, j+1,tmp);
		
	 }
}
