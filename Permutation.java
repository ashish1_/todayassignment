package com.StringPermutation;

import java.util.ArrayList;
import java.util.Scanner;



public class Permutation {
	

	public static void main(String[] args){
		ArrayList<String> itemList = new ArrayList<>();
		Scanner scan=new Scanner(System.in);
		String string;
		System.out.println("Enter any String: ");
		string=scan.next();
		
		permute(string ,"", itemList);
		System.out.println(itemList);
	}

	private static void permute(String input,String string,ArrayList<String> itemList) {
		if(input.equals("")){
			itemList.add(string);
		}
		for(int i=0;i<input.length();i++){
			char character=input.charAt(i);
			if(input.indexOf(character,i+1)!=-1){	
				continue;
			}
			permute(input.substring(0,i)+input.substring(i+1), string+character, itemList);
		}
	}
}
