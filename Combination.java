package com.StringPermutation;

import java.util.Scanner;

public class Combination {
	StringBuilder output=new StringBuilder();
	String inputString;
	Scanner scan =new Scanner(System.in);
	public Combination() {
		System.out.println("Enter any String: ");
		inputString=scan.next();
		
	}
	public void start(int initial) {
		for(int i=initial;i<inputString.length();++i){
			output.append(inputString.charAt(i));
			System.out.println(output);
			if(i<inputString.length())
				start(i+1);
			output.setLength(output.length()-1);
		}
	}
}
